def test_home_page(client):
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"data": "Welcome to the cooking book!"}


def test_list_dishes(dishes, client):
    response = client.get("/dishes/")
    assert response.status_code == 200
    assert len(response.json()) == 2
    response = client.get("/dishes/1")
    assert response.status_code == 200
    data = response.json()
    assert data["views"] == 1
    response = client.get("/dishes/1")
    data = response.json()
    assert response.status_code == 200
    assert data["views"] == 2
    assert data["cooking_time"] == 10
    assert data["name"] == "dish_1"
    assert data["description"] == "desc_1"
    response = client.get("/dishes/10")
    assert response.status_code == 404
