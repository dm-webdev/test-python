from ..models import Dishes, DishIngredients, Ingredients

dishes = [
    {
        "id": 1,
        "name": "dish_1",
        "description": "desc_1",
        "cooking_time": 10,
        "views": 0,
    },
    {
        "id": 2,
        "name": "dish_2",
        "description": "desc_2",
        "cooking_time": 20,
        "views": 20,
    },
]

test_dishes = [Dishes(**dish) for dish in dishes]

ingredients = [
    {"id": 1, "name": "ingredient_1"},
    {"id": 2, "name": "ingredient_2"},
    {"id": 3, "name": "ingredient_3"},
    {"id": 4, "name": "ingredient_4"},
]

test_ingredients = [Ingredients(**ingredient) for ingredient in ingredients]

dish_ingredients = [
    {"dish_id": 1, "ingredient_id": 1},
    {"dish_id": 1, "ingredient_id": 2},
    {"dish_id": 1, "ingredient_id": 3},
    {"dish_id": 2, "ingredient_id": 4},
    {"dish_id": 2, "ingredient_id": 1},
]

test_dish_ingredients = [
    DishIngredients(**ingredient) for ingredient in dish_ingredients
]
