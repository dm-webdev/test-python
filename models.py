from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .database import Base


class Dishes(Base):
    __tablename__ = "dishes"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(200), nullable=False, index=True)
    description = Column(String(1000), nullable=False)
    cooking_time = Column(Integer, nullable=False, index=False)
    views = Column(Integer, default=0)
    ingredients = relationship(
        "DishIngredients", back_populates="dish", cascade="all, delete", lazy="select"
    )


class Ingredients(Base):
    __tablename__ = "ingredients"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(200), nullable=False, index=True)
    dishes = relationship(
        "DishIngredients",
        back_populates="ingredient",
        cascade="all, delete-orphan",
        lazy="selectin",
    )


class DishIngredients(Base):
    __tablename__ = "dish_ingredients"

    dish_id = Column(
        Integer, ForeignKey("dishes.id"), nullable=False, index=True, primary_key=True
    )
    ingredient_id = Column(
        Integer,
        ForeignKey("ingredients.id"),
        nullable=False,
        index=True,
        primary_key=True,
    )
    ingredient = relationship("Ingredients", back_populates="dishes", lazy="selectin")
    dish = relationship("Dishes", back_populates="ingredients", lazy="selectin")
