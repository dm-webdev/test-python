from typing import Dict, List

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from . import crud, models
from .database import SessionLocal, engine
from .initial_data import initial_dish_ingredients, initial_dishes, initial_ingredients
from .schemas import DishesOut, DishOut

models.Base.metadata.create_all(bind=engine)
app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.on_event("startup")
def shutdown():
    db = SessionLocal()
    try:
        dishes = db.execute(select(models.Dishes))
        if not dishes:
            db.add_all(initial_dishes)
            db.add_all(initial_ingredients)
            db.add_all(initial_dish_ingredients)
            db.commit()
    finally:
        db.close()


@app.get("/")
async def home() -> Dict[str, str]:
    return {"data": "Welcome to the cooking book!"}


@app.get("/dishes/", response_model=List[DishesOut])
async def get_dishes(db: Session = Depends(get_db)) -> List[DishesOut]:
    return crud.get_dishes(db=db)


@app.get("/dishes/{dish_id}", response_model=DishOut)
def get_dish(dish_id: int, db: Session = Depends(get_db)) -> DishOut:
    dish = crud.get_dish(db, dish_id=dish_id)
    if dish is None:
        raise HTTPException(status_code=404, detail="Dish not found")
    return dish
