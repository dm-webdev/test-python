from typing import List

from pydantic import BaseModel


class BaseIngredient(BaseModel):
    id: int
    name: str

    class Config:
        orm_mode = True


class BaseIngredientOut(BaseModel):
    ingredient_id: int
    dish_id: int
    ingredient: BaseIngredient

    class Config:
        orm_mode = True


class BaseDish(BaseModel):
    name: str
    cooking_time: int
    views: int


class DishesOut(BaseDish):
    id: int

    class Config:
        orm_mode = True


class DishOut(BaseDish):
    id: int
    description: str
    ingredients: List[BaseIngredientOut]

    class Config:
        orm_mode = True
