from .models import Dishes, DishIngredients, Ingredients

dishes = [
    {
        "id": 1,
        "name": "Сырники из творога",
        "description": "Главный секрет идеальных сырников — а точнее "
                       "творожников, — творог нужно протереть через "
                       "мелкое сито и отжать от влаги. Жирность "
                       "предпочтительна не больше и не меньше 9%.",
        "cooking_time": 30,
        "views": 10,
    },
    {
        "id": 2,
        "name": "Спагетти карбонара с красным луком",
        "description": "Спагетти карбонара — хоть блюдо и итальянское, "
                       "оно имеет хорошую популярность во всем мире, в том "
                       "числе и у нас. Изобретенная когда-то простыми "
                       "шахтерами, эта простая и сытная паста завоевала "
                       "сердца и желудки многих.",
        "cooking_time": 20,
        "views": 5,
    },
    {
        "id": 3,
        "name": "Классическая шарлотка",
        "description": "Классическая шарлотка. Важное сладкое блюдо советской "
                       "и постсоветской истории. Легкое, пышное тесто, "
                       "максимум яблочной начинки — у шарлотки всегда был "
                       "образ приятного, простого и при этом лакомого и "
                       "диетического блюда.",
        "cooking_time": 35,
        "views": 50,
    },
    {
        "id": 4,
        "name": "Лазанья классическая с мясом",
        "description": "Делала пропорции на 4 порции. Очень вкусно!",
        "cooking_time": 40,
        "views": 35,
    },
    {
        "id": 5,
        "name": "Тонкие блины на молоке",
        "description": "Тонкие блины на молоке — это английский вариант "
                       "традиционных пышных русских блинов, выпеченных на "
                       "дрожжах. В Европе блинчики имеют вид тонких, почти "
                       "прозрачных салфеток. ",
        "cooking_time": 60,
        "views": 90,
    },
]

initial_dishes = [Dishes(**dish) for dish in dishes]

ingredients = [
    {"id": 1, "name": "Творог"},
    {"id": 2, "name": "Куриное яйцо"},
    {"id": 3, "name": "Пшеничная мука"},
    {"id": 4, "name": "Сахар"},
    {"id": 5, "name": "Подсолнечное масло"},
    {"id": 6, "name": "Спагетти"},
    {"id": 7, "name": "Сливочное масло"},
    {"id": 8, "name": "Чеснок"},
    {"id": 9, "name": "Красный лук"},
    {"id": 10, "name": "Тертый сыр пармезан"},
    {"id": 11, "name": "Соль"},
    {"id": 12, "name": "Молотый черный перец"},
    {"id": 13, "name": "Яблоко"},
    {"id": 14, "name": "Разрыхлитель"},
    {"id": 15, "name": "Мясной фарш"},
    {"id": 16, "name": "Соус болоньезе"},
    {"id": 17, "name": "Молоко"},
    {"id": 18, "name": "Готовые сухие листы лазаньи"},
    {"id": 19, "name": "Твердый сыр"},
]

initial_ingredients = [Ingredients(**ingredient) for ingredient in ingredients]

dish_ingredients = [
    {"dish_id": 1, "ingredient_id": 1},
    {"dish_id": 1, "ingredient_id": 2},
    {"dish_id": 1, "ingredient_id": 3},
    {"dish_id": 1, "ingredient_id": 4},
    {"dish_id": 1, "ingredient_id": 5},
    {"dish_id": 2, "ingredient_id": 2},
    {"dish_id": 2, "ingredient_id": 6},
    {"dish_id": 2, "ingredient_id": 7},
    {"dish_id": 2, "ingredient_id": 8},
    {"dish_id": 2, "ingredient_id": 9},
    {"dish_id": 2, "ingredient_id": 10},
    {"dish_id": 2, "ingredient_id": 11},
    {"dish_id": 2, "ingredient_id": 12},
    {"dish_id": 3, "ingredient_id": 4},
    {"dish_id": 3, "ingredient_id": 2},
    {"dish_id": 3, "ingredient_id": 3},
    {"dish_id": 3, "ingredient_id": 13},
    {"dish_id": 3, "ingredient_id": 5},
    {"dish_id": 3, "ingredient_id": 14},
    {"dish_id": 4, "ingredient_id": 15},
    {"dish_id": 4, "ingredient_id": 16},
    {"dish_id": 4, "ingredient_id": 7},
    {"dish_id": 4, "ingredient_id": 3},
    {"dish_id": 4, "ingredient_id": 5},
    {"dish_id": 4, "ingredient_id": 17},
    {"dish_id": 4, "ingredient_id": 18},
    {"dish_id": 4, "ingredient_id": 19},
    {"dish_id": 5, "ingredient_id": 3},
    {"dish_id": 5, "ingredient_id": 4},
    {"dish_id": 5, "ingredient_id": 2},
    {"dish_id": 5, "ingredient_id": 17},
    {"dish_id": 5, "ingredient_id": 11},
    {"dish_id": 5, "ingredient_id": 5},
]

initial_dish_ingredients = [
    DishIngredients(**ingredient) for ingredient in dish_ingredients
]
