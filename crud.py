from sqlalchemy.orm import Session

from . import models


def get_dishes(db: Session):
    return db.query(models.Dishes).all()


def get_dish(db: Session, dish_id: int):
    dish = db.query(models.Dishes).filter(models.Dishes.id == dish_id).first()
    if dish:
        dish.views += 1
        db.commit()
    return dish
